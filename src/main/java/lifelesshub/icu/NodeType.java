package lifelesshub.icu;

public enum NodeType {
    DEFAULT,
    BARRIER,
    SOURCE,
    DESTINATION,
    MARKED,
    INSPECTED,
    TRACED
}
